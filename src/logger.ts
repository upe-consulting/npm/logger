import * as chalk from 'chalk';
import * as stringify from 'json-stringify-safe';
import * as prettyjson from 'prettyjson';
import { ALL_TYPES, Log, LogSeverityLevel, LogType } from './log';
import { contain, getRandomColor, removeFromArray } from './utils';

export interface ILoggable {
  logger: Logger;
}

export interface IConsole {
  log(message?: any, ...optionalParams: any[]): void;
}

export interface ILoggerSettings {
  color?: string;
  console?: IConsole;
  fixedWidth?: number;
  flags?: string[];
  types?: LogType[];
  severity?: LogSeverityLevel[];
  muted?: boolean;
  name: string;
}

export interface IHttpLogger {
  log(log: Log): void;
}

export class Logger implements ILoggerSettings {

  public static MuteAllByFlags(...flags: string[]): void {
    Logger.INSTANCES
      .filter((logger: Logger) => flags.filter((flag: string) => logger.hasFlag(flag)).length !== 0)
      .forEach((logger: Logger) => {
        logger.mute();
      });
  }

  public static MuteType(...types: LogType[]): void {
    types.forEach((type: LogType) => {
      if (!contain(Logger.MUTED_TYPES, type)) {
        Logger.MUTED_TYPES.push(type);
      }
    });
  }

  public static UnMuteAllByFlags(...flags: string[]): void {
    Logger.INSTANCES
      .filter((logger: Logger) => flags.filter((flag: string) => logger.hasFlag(flag)).length !== 0)
      .forEach((logger: Logger) => {
        logger.unMute();
      });
  }

  public static UnMuteType(...types: LogType[]): void {
    types.forEach((type: LogType) => {
      removeFromArray(Logger.MUTED_TYPES, type);
    });
  }

  public static NODE: boolean                   = false;
  public static HTTP_LOGGER: IHttpLogger | null = null;
  public static INSTANCES: Logger[]             = [];
  public static MUTED: boolean                  = false;
  public static MUTED_TYPES: LogType[]          = [];
  public static DISABLED: boolean               = false;
  public color: string                          = getRandomColor();
  public console: Console                       = console;
  public fixedWidth: number                     = -1;
  public flags: string[]                        = [];
  public types: LogType[]                       = ALL_TYPES();
  public muted: boolean                         = false;
  public name: string;

  public constructor(settings: ILoggerSettings | null = null) {
    let s: ILoggerSettings;
    if (settings === null) {

      const stackTrace = (new Error()).stack || ''; // Only tested in latest FF and Chrome
      let callerName   = stackTrace.replace(/^Error\s+/, ''); // Sanitize Chrome
      callerName  = callerName.split('\n')[1]; // 1st item is this, 2nd item is caller
      callerName  = callerName.replace(/^\s+at Object./, ''); // Sanitize Chrome
      callerName  = callerName.replace(/ \(.+\)$/, ''); // Sanitize Chrome
      callerName  = callerName.replace(/new\s/, ''); // Sanitize Chrome
      callerName  = callerName.replace(/\@.+/, ''); // Sanitize Firefox
      callerName  = callerName.replace(/^\s+at\s/, '');
      callerName  = callerName.replace(/\.use\s.*/, '');
      callerName  = callerName.split(' ')[0];
      const flags = callerName.match(/([A-Z][a-z]*)*/) || [];
      delete flags.index;
      delete flags.input;
      s = { name: callerName, flags: flags as string[] };
    } else {
      s = settings;
    }
    // tslint:disable:no-string-literal
    Object['assign'](this, s);
    Logger.INSTANCES.push(this);
  }

  // region flags

  public hasFlag(flag: string): boolean {
    return contain(this.flags, flag);
  }

  public removeFlag(flag: string): void {
    removeFromArray(this.flags, flag);
  }

  public addFlag(flag: string): void {
    this.flags.push(flag);
  }

  public addLogType(type: LogType): void {
    if (!contain(this.types, type)) {
      this.types.push(type);
    }
  }

  // endregion

  // region types

  public hasLogType(type: LogType): boolean {
    return contain(this.types, type);
  }

  public removeLogType(type: LogType): void {
    removeFromArray(this.types, type);
  }

  // endregion

  // region mute

  public mute(): void {
    this.muted = true;
  }

  public unMute(): void {
    this.muted = false;
  }

  // endregion

  public debug(msg: string, data: any | null = null, severity: LogSeverityLevel = LogSeverityLevel.MEDIUM): void {
    this.logMessage(msg, LogType.DEBUG, data, severity);
  }

  public info(msg: string, data: any | null = null, severity: LogSeverityLevel = LogSeverityLevel.MEDIUM): void {
    this.logMessage(msg, LogType.INFO, data, severity);
  }

  public warn(msg: string, data: any | null = null, severity: LogSeverityLevel = LogSeverityLevel.MEDIUM): void {
    this.logMessage(msg, LogType.WARN, data, severity);
  }

  public error(msg: string, data: any | null = null, severity: LogSeverityLevel = LogSeverityLevel.MEDIUM): void {
    this.logMessage(msg, LogType.ERROR, data, severity);
  }

  public fatal(msg: string, data: any | null = null, severity: LogSeverityLevel = LogSeverityLevel.MEDIUM): void {
    this.logMessage(msg, LogType.FATAL, data, severity);
  }

  private logMessage(msg: string, type: LogType, data: any | null = null, severity: LogSeverityLevel): void {
    if (Logger.DISABLED) {
      return;
    }
    let saveData: object | null = null;
    if (data !== null) {
      let objectData: any = data;
      if (typeof data !== 'object') {
        objectData = { data };
      }
      try {
        saveData = JSON.parse(stringify(objectData));
      } catch (e) {
        saveData = {};
      }
    }

    if (Logger.HTTP_LOGGER) {
      const log    = new Log();
      log.message  = msg;
      log.type     = type;
      log.severity = severity;
      log.data     = saveData;
      Logger.HTTP_LOGGER.log(log);
    }
    if (!this.muted && !contain(Logger.MUTED_TYPES, type) && !Logger.MUTED) {

      if (this.types.filter((item: LogType) => item === type).length !== 0) {
        this.printMsg(msg, saveData, type);
      }

    }
  }

  private printMsg(
    msg: string,
    data: object | null,
    type: LogType,
  ): void {
    if (Logger.NODE) {

      let name = `[${this.name}]`;

      const options = {
        noColor: false,
      };

      switch (type) {
        case LogType.DEBUG:
          name = chalk.black(name);
          break;

        case LogType.ERROR:
          name = chalk.red(name);
          break;

        case LogType.INFO:
          name = chalk.blue(name);
          break;

        case LogType.WARN:
          name = chalk.magenta(name);
          break;

        default:
          name = chalk.gray(name);
      }

      const log = `${name} : ${msg}`;

      if (data !== null) {
        this.console.log(log);
        this.console.log(prettyjson.render(data, options));
      } else {
        this.console.log(log);
      }

    } else {

      let color = 'gray';
      if (type === LogType.INFO) {
        color = 'deepskyblue';
      }
      if (type === LogType.ERROR) {
        color = 'red';
      }
      if (type === LogType.WARN) {
        color = 'orange';
      }
      if (type === LogType.DEBUG) {
        color = 'black';
      }

      let name = this.name;

      if (this.fixedWidth !== -1) {
        const diff = this.fixedWidth - this.name.length;
        if (diff > 0) {
          for (let i = 0; i < diff; i++) {
            name += ' ';
          }
        }
      }

      const consoleData: any[] = data !== null ? [data] : [];

      consoleData.unshift(`border: 1px solid ${color}; `);
      consoleData.unshift(`background: ${this.color};color:white; border: 1px solid ${this.color}; `);
      consoleData.unshift(`%c ${name}  %c ${msg} `);
      this.console.log.apply(this.console, consoleData);
    }
  }

}
