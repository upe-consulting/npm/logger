/* tslint:disable:object-literal-shorthand */
/* tslint:disable:no-unused-expression */

import { expect } from 'chai';
import { suite, test } from 'mocha-typescript';
import * as sinon from 'sinon';
import { ALL_TYPES, Logger, LogType } from '../src';

@suite
class LoggerTest {

  @test
  public 'add flag'(): void {

    const logger = new Logger({name: 'test-add-flag'});

    expect(logger.flags).has.length(0);

    logger.addFlag('test-add-flag');

    expect(logger.flags).has.length(1);

    expect(logger.flags[0]).eq('test-add-flag');

  }

  @test
  public 'add type'(): void {
    const logger = new Logger({ name: 'test-add-type', types: [] });

    expect(logger.types).has.length(0);

    logger.addLogType(LogType.DEBUG);

    expect(logger.types).has.length(1);

    expect(logger.types[0]).eq(LogType.DEBUG);

    logger.addLogType(LogType.DEBUG);

    expect(logger.types).has.length(1);
  }

  @test
  public 'debug'(): void {

    const log = sinon.spy();

    const logger = new Logger({ name: 'test-debug', console: { log: log } });

    logger.debug('msg', {});

    expect(log.calledOnce).to.be.true;

    logger.mute();

    logger.debug('msg', {});

    expect(log.calledOnce).to.be.true;

    logger.unMute();

    logger.debug('msg', {});

    expect(log.calledTwice).to.be.true;

    logger.removeLogType(LogType.DEBUG);

    logger.debug('msg', {});

    expect(log.calledTwice).to.be.true;

  }

  @test
  public 'error'(): void {
    const log = sinon.spy();

    const logger = new Logger({name: 'test-error', console: {log: log}});

    logger.error('msg', {});

    expect(log.calledOnce).to.be.true;

    logger.mute();

    logger.error('msg', {});

    expect(log.calledOnce).to.be.true;

    logger.unMute();

    logger.error('msg', {});

    expect(log.calledTwice).to.be.true;

    logger.removeLogType(LogType.ERROR);

    logger.error('msg', {});

    expect(log.calledTwice).to.be.true;
  }

  @test
  public 'has flag'(): void {

    const logger = new Logger({name: 'test-has-flag', flags: ['test-has-flag']});

    expect(logger.flags[0]).eq('test-has-flag');

    expect(logger.hasFlag('test-has-flag')).to.be.true;

  }

  @test
  public 'has type'(): void {

    const logger = new Logger({ name: 'test-has-type', types: [LogType.DEBUG] });

    expect(logger.types[0]).eq(LogType.DEBUG);

    expect(logger.hasLogType(LogType.DEBUG)).to.be.true;

  }

  @test
  public 'info'(): void {
    const log = sinon.spy();

    const logger = new Logger({name: 'test-info', console: {log: log}});

    logger.info('msg');

    expect(log.calledOnce).to.be.true;

    logger.mute();

    logger.info('msg');

    expect(log.calledOnce).to.be.true;

    logger.unMute();

    logger.info('msg');

    expect(log.calledTwice).to.be.true;

    logger.removeLogType(LogType.INFO);

    logger.info('msg');

    expect(log.calledTwice).to.be.true;
  }

  @test
  public 'fatal'(): void {
    const log = sinon.spy();

    const logger = new Logger({ name: 'test-fatal', console: { log: log } });

    logger.fatal('msg');

    expect(log.calledOnce).to.be.true;

    logger.mute();

    logger.fatal('msg');

    expect(log.calledOnce).to.be.true;

    logger.unMute();

    logger.fatal('msg');

    expect(log.calledTwice).to.be.true;

    logger.removeLogType(LogType.FATAL);

    logger.fatal('msg');

    expect(log.calledTwice).to.be.true;
  }

  @test
  public 'mute'(): void {

    const logger = new Logger({name: 'test-mute'});

    expect(logger.muted).to.be.false;

    logger.mute();

    expect(logger.muted).to.be.true;

  }

  @test
  public 'remove falg'(): void {

    const logger = new Logger({name: 'test-remove-flag', flags: ['test-remove-flag']});

    logger.removeFlag('test-remove-flag');

    expect(logger.hasFlag('test-remove-flag')).to.be.false;

  }

  @test
  public 'remove type'(): void {

    const logger = new Logger({ name: 'test-remove-type', types: [LogType.DEBUG] });

    logger.removeLogType(LogType.DEBUG);

    expect(logger.hasLogType(LogType.DEBUG)).to.be.false;
  }

  @test
  public 'unMute'(): void {

    const logger = new Logger({name: 'test-un-mute', muted: true});

    expect(logger.muted).to.be.true;

    logger.unMute();

    expect(logger.muted).to.be.false;

  }

  @test
  public 'warn'(): void {
    const log = sinon.spy();

    const logger = new Logger({name: 'test-warn', console: {log: log}});

    logger.warn('msg', {});

    expect(log.calledOnce).to.be.true;

    logger.mute();

    logger.warn('msg', {});

    expect(log.calledOnce).to.be.true;

    logger.unMute();

    logger.warn('msg', {});

    expect(log.calledTwice).to.be.true;

    logger.removeLogType(LogType.WARN);

    logger.warn('msg', {});

    expect(log.calledTwice).to.be.true;
  }

  @test
  public 'fixed width'(): void {

    const logger = new Logger({
      console: {log: (...args: any[]): any => args.splice(0, 0)},
      fixedWidth: 100,
      name: 'fixed-width',
    });

    logger.info('msg');

    logger.fixedWidth = 1;

    expect(logger.fixedWidth).eq(1);

    logger.info('msg');

    const fixedWidth = 500;

    logger.fixedWidth = fixedWidth;

    expect(logger.fixedWidth).eq(fixedWidth);

    logger.info('msg');

  }

  @test
  public 'set color'(): void {

    const logger = new Logger({name: 'set-color'});

    logger.color = 'red';

    expect(logger.color).eq('red');

  }

  @test
  public 'mute all'(): void {

    Logger.MUTED = true;

    expect(Logger.MUTED).true;

    const log = sinon.spy();

    const logger = new Logger({name: 'mute-all', console: {log: log}});

    logger.info('');
    logger.debug('');
    logger.warn('');
    logger.error('');
    logger.fatal('');

    expect(log.callCount).to.be.eq(0);

  }

  @test
  public 'un mute all'(): void {

    Logger.MUTED = false;

    expect(Logger.MUTED).false;

    const log = sinon.spy();

    const logger = new Logger({name: 'un-mute-all', console: {log: log}});

    logger.info('');
    logger.debug('');
    logger.warn('');
    logger.error('');
    logger.fatal('');

    const callCount = 5;

    expect(log.callCount).to.be.eq(callCount);

  }

  @test
  public 'mute all by flag'(): void {

    const five = 5;

    for (let i = 0; i < five; i++) {
      new Logger({name: `test-mute-all-by-flag-${i}`, flags: ['test-mute-all-by-flag']});
    }

    expect(Logger.INSTANCES.filter((logger: Logger) =>
      logger.hasFlag('test-mute-all-by-flag') && !logger.muted)).has.length(five);

    Logger.MuteAllByFlags('test-mute-all-by-flag');

    expect(Logger.INSTANCES.filter((logger: Logger) =>
      logger.hasFlag('test-mute-all-by-flag') && logger.muted)).has.length(five);

  }

  @test
  public 'un mute all by flag'(): void {

    const five = 5;

    Logger.UnMuteAllByFlags('test-mute-all-by-flag');

    expect(Logger.INSTANCES.filter((logger: Logger) =>
      logger.hasFlag('test-mute-all-by-flag') && !logger.muted)).has.length(five);

  }

  @test
  public 'mute type'(): void {

    const log = sinon.spy();

    const logger = new Logger({ name: 'mute-type', console: { log: log } });

    Logger.MuteType(...ALL_TYPES());
    Logger.MuteType(...ALL_TYPES());

    expect(Logger.MUTED_TYPES).has.length(ALL_TYPES().length);

    logger.info('');
    logger.debug('');
    logger.warn('');
    logger.error('');
    logger.fatal('');

    expect(log.called).to.be.false;
  }

  @test
  public 'un mute type'(): void {

    const log = sinon.spy();

    const logger = new Logger({ name: 'un-mute-type', console: { log: log } });

    Logger.UnMuteType(...ALL_TYPES());

    expect(Logger.MUTED_TYPES).has.length(0);

    logger.info('');
    logger.debug('');
    logger.warn('');
    logger.error('');
    logger.fatal('');

    expect(log.callCount).to.be.eq(ALL_TYPES().length);
  }

  @test
  public 'Node mode'(): void {

    Logger.NODE = true;

    const log = sinon.spy();
    const msg = 'msg';
    const data = {test: 'data', test2: 3, test3: false};

    const logger = new Logger({name: 'node-mode', console: {log: log}});

    expect(log.callCount).to.be.eq(0);

    logger.info(msg, data);
    logger.debug(msg, data);
    logger.warn(msg, data);
    logger.error(msg, data);
    logger.fatal(msg, data);

    expect(log.callCount).to.be.eq(ALL_TYPES().length * 2);
  }

  @test
  public 'Node mode visual'(): void {

    Logger.NODE = true;

    const msg = 'msg';
    const data = {test: 'data', test2: 3, test3: false};

    const logger = new Logger({name: 'node-mode'});

    logger.info(msg);
    logger.debug(msg, data);
    logger.warn(msg, data);
    logger.error(msg, data);
    logger.fatal(msg);

  }

  @test
  public 'use calling class name'(): void {

    // tslint:disable:max-classes-per-file
    class LoggerAutoName {

      public logger: Logger;

      public constructor() {
        this.logger = new Logger();
      }

    }

    const l = new LoggerAutoName();

    expect(l.logger.name).eq('LoggerAutoName');

    // tslint:disable:max-classes-per-file
    class LoggerAutoName1 {

      public logger: Logger = new Logger();

      public test: string;

      public constructor() {
        this.test = 'test';
      }

    }

    const l1 = new LoggerAutoName1();

    expect(l1.logger.name).eq('LoggerAutoName1');

    // tslint:disable:max-classes-per-file
    class LoggerAutoName2 {

      public logger: Logger = new Logger();

      public constructor(public test2: string) {
      }

    }

    const l2 = new LoggerAutoName2('test');

    expect(l2.logger.name).eq('LoggerAutoName2');

  }

}
