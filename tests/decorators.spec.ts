import { suite, test } from 'mocha-typescript';
import { Demo } from '../demo';
import { Logger } from '../src';

@suite
class DecoratorsSpec {

  public static before(): void {
    Logger.NODE = true;
  }

  public static after(): void {
    Logger.NODE = false;
  }

  @test
  public 'test'(): void {

    const d = new Demo();

    d.cleanAll('my');

    const t = d.test;

  }

}
