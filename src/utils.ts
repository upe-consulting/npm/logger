export function contain(array: any[], value: any): boolean {
  return array.filter((item: any) => item === value).length !== 0;
}

export function getRandomColor(): any {
  const five    = 5;
  const fifteen = 15;
  let letters   = '012345'.split('');
  let color     = '#';
  color += letters[Math.round(Math.random() * five)];
  letters       = '0123456789ABCDEF'.split('');
  for (let i = 0; i < five; i++) {
    color += letters[Math.round(Math.random() * fifteen)];
  }

  return color;
}

export function removeFromArray(array: any[], value: any): void {
  for (let i = 0; i < array.length; i++) {
    if (array[i] === value) {
      array.splice(i, 1);
      i--;
    }
  }
}
