export enum LogType {
  DEBUG = 0,
  INFO  = 1,
  WARN  = 2,
  ERROR = 3,
  FATAL = 4,
}

export const ALL_TYPES = (): LogType[] => [LogType.DEBUG, LogType.INFO, LogType.WARN, LogType.ERROR, LogType.FATAL];

export enum LogSeverityLevel {
  LOW      = 0,
  MEDIUM   = 1,
  HIGH     = 2,
  CRITICAL = 3,
}

export class Log {

  public static DEFAULT_TYPE: LogType              = LogType.INFO;
  public static DEFAULT_SEVERITY: LogSeverityLevel = LogSeverityLevel.MEDIUM;

  public timeStamp: number          = Date.now();
  public type: LogType              = Log.DEFAULT_TYPE;
  public severity: LogSeverityLevel = Log.DEFAULT_SEVERITY;
  public message: string;
  public data: object | null        = null;

}
