[![build status](https://gitlab.com/upe-consulting/npm/logger/badges/master/build.svg)](https://gitlab.com/upe-consulting/npm/logger/commits/master)
[![coverage report](https://gitlab.com/upe-consulting/npm/logger/badges/master/coverage.svg)](https://gitlab.com/upe-consulting/npm/logger/commits/master)

# Logger
A simple named logger for typescript (based on the idea of ng2-logger and typescript-logger)

> Currently npm don't support gitlabs subgroups!
>
> [GitLab Repo](https://gitlab.com/upe-consulting/npm/logger)
> | [Issues](https://gitlab.com/upe-consulting/npm/logger/issues)
>
> `git@gitlab.com:upe-consulting/npm/logger.git`

### Change Log

###### v1.0.1
+ new Log Levels
+ HTTP Logger interface
+ Save stringify

###### v0.3.3
+ remove console.log call

###### v0.3.1
+ add data param to all types

###### v0.3.0
+ add node logger

###### v0.2.2
+ fix un/mute all

###### v0.2.1

+ add interface ILoggable

###### v0.2.0

+ add MuteType
+ add UnMuteType

###### v0.1.x

+ remove DEFAULT_LOGGER_SETTINGS
+ change instances type to Logger[]
+ add hasLevel
+ add removeFlag
+ add removeLevel
+ add tests

##### v0.0.x

+ add basic functionalists

### Usage
```typescript
import {ILoggerSettings, Logger} from '@upe/logger';

let settings: ILoggerSettings = {
  name: 'MyLoggerName'
};

let logger = new Logger(settings);

logger.info('test info');
logger.log('test log');
logger.data('test data');
logger.warn('test warning');
logger.error('test error');

logger.mute();
logger.unMute();

// un/mute all logger
Logger.MuteAll();
Logger.UnMuteAll();

// un/mute all logger by flag
Logger.MuteAllByFlags('flag2', 'flag1');
Logger.UnMuteAllByFlags('flag2', 'flag1', 'flag0');

```

#### ILoggerSettings

name | type |description
--- | --- | ---
color | string | Main `Logger` color
console | Console | The instance of the used console
fixedWidth | number | fixed with of the output (0 = infinity)
flags | string[] | Flags to group `Logger` instances
types | Level[] | Levels where the `Logger` can print
muted | boolean | If set the `Logger` is muted
name | string | Name of the `Logger` instance

#### Level

 + DATA
 + INFO
 + LOG
 + WARN
 + ERROR

