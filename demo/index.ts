import { ILoggable, LogClass, Logger } from '../src';

@LogClass()
export class Demo implements ILoggable {

  public get test(): string {
    return this._test + this._test;
  }

  public logger: Logger = new Logger();
  private _test: string = 'one';

  public cleanAll(test: string) {
    return test + 'test';
  }

}
