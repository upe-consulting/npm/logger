import { ILoggable } from './logger';

export const DEFAULT_SKIP_METHODS: string[] = ['constructor'];
export const ANGULAR_SKIP_METHODS: string[] = [
  'ngOnChanges',
  'ngOnInit',
  'ngDoCheck',
  'ngAfterContentInit',
  'ngAfterContentChecked',
  'ngAfterViewInit',
  'ngAfterViewChecked',
  'ngOnDestroy',
];

export const DECORATOR_CONFIG: { disabled: boolean } = { disabled: false };

export type ConstructorType<T> = new (...args: any[]) => T;

export function LogClass(skip: string[] = []): any {
  return function (target: ConstructorType<ILoggable>): any {
    return applyClass(target, skip.concat(DEFAULT_SKIP_METHODS));
  };
}

export function LogComponent(skip: string[] = []): any {
  return function (target: ConstructorType<ILoggable>): any {
    return applyClass(target, skip.concat(DEFAULT_SKIP_METHODS, ANGULAR_SKIP_METHODS));
  };
}

export function LogDirective(skip: string[] = []): any {
  return function (target: ConstructorType<ILoggable>): any {
    return applyClass(target, skip.concat(DEFAULT_SKIP_METHODS, ANGULAR_SKIP_METHODS));
  };
}

export function LogInjectable(skip: string[] = []): any {
  return function (target: ConstructorType<ILoggable>): any {
    return applyClass(target, skip.concat(DEFAULT_SKIP_METHODS));
  };
}

export function LogMethod(): any {
  return function (target: ConstructorType<ILoggable>, propertyKey: string, descriptor: PropertyDescriptor): any {
    return applyMethod(propertyKey, descriptor);
  };
}

export function LogProperty(): any {
  return function (target: ConstructorType<ILoggable>, propertyKey: string, descriptor: PropertyDescriptor): any {
    return applyProperty(propertyKey, descriptor);
  };
}

function applyFunction(propertyKey: string, descriptor: PropertyDescriptor): PropertyDescriptor {
  return descriptor.value ? applyMethod(propertyKey, descriptor) : applyProperty(propertyKey, descriptor);
}

function applyClass(target: ConstructorType<ILoggable>, skip: string[]): ConstructorType<ILoggable> {

  if (DECORATOR_CONFIG.disabled) {
    return target;
  }

  const names = Object.getOwnPropertyNames(target.prototype)
    .filter((name: string) => skip.indexOf(name) === -1);

  for (const name of names) {
    const descriptor = applyFunction(name, Object.getOwnPropertyDescriptor(target.prototype, name));
    Object.defineProperty(target.prototype, name, descriptor);
  }

  return target;
}

function applyProperty(propertyKey: string, descriptor: PropertyDescriptor): PropertyDescriptor {

  if (DECORATOR_CONFIG.disabled || propertyKey === 'logger') {
    return descriptor;
  }

  const get = descriptor.get;
  const set = descriptor.set;

  if (get) {
    descriptor.get = function (): any {
      const result = get.apply(this);

      this.logger.debug(`get ${propertyKey}`, { result });

      return result;

    };
  }

  if (set) {
    descriptor.set = function (value: any) {
      this.logger.debug(`set ${propertyKey}`, { value });
      set.apply(this, value);
    };
  }

  return descriptor;

}

function applyMethod(propertyKey: string, descriptor: PropertyDescriptor): PropertyDescriptor {

  if (DECORATOR_CONFIG.disabled) {
    return descriptor;
  }

  const method = descriptor.value;

  // editing the descriptor/value parameter
  descriptor.value = function () {
    const args: any[] = [];
    for (let i = 0; i < arguments.length; i++) {
      args[i] = arguments[i];
    }
    const result = method.apply(this, args);

    this.logger.debug(propertyKey, { args, result });

    return result;
  };

  // return edited descriptor as opposed to overwriting the descriptor
  return descriptor;
}
